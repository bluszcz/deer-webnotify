#!/usr/bin/env pyton

import sys
import json
import sqlite3

import requests

def send_telegram_notify(msg):
    from telegram_send import send
    send(messages=[msg])

def check_if_exists(url, value, notify="print", desc=''):
    text = requests.get(url).text
    result = text.find(value)
    _notify = eval(notify)
    if result!=-1:
        _notify("Found %s at %s" % (value, url))
    else:
        print("Not found")

def _get_cursor(config_file):
    sqlite_file = config_file + ".db"
    return sqlite3.connect(sqlite_file).cursor()


def main():
    if len(sys.argv)>1:
            config_file = sys.argv[1]
            json_config = json.loads(open(sys.argv[1]).read())
            c = _get_cursor(config_file)

            c.execute('''CREATE TABLE IF NOT EXISTS statuses
                 (hash text, url text, value text, status real)''')
            for elem in json_config:
                url = elem['url']
                value = elem['value']
                notify = elem['notify']
                check_if_exists(**elem)
                print(elem)
            return 0
    else:
        print("Launch: %s config.json" % sys.argv[0])
        return 1

if __name__=="__main__":
    sys.exit(main())
